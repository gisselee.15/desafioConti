import React, { Component } from 'react';
import '../css/Login.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';
//import md5 from 'md5';
import Cookies from 'universal-cookie';
import { useFormik } from 'formik';


const baseUrl="http://localhost:3001/login";
const cookies = new Cookies();
const SignupForm = () => {

    const formik= useFormik({
        initialValues:{
            username:'',
            pasword:''
        },
        onSubmit: values => {
            alert(JSON.stringify(values));
        }
    });
}
const validateLogin=loginData=> {
    const errors={};
    if(!loginData.username){
        errors.username='Please enter username';
    }
    if(!loginData.password){
        errors.password='Please enter password';
    }
    return errors;
}

class Login extends Component {
    
    state={
        form:{
            username: '',
            password: ''
        }
    }

    handleChange=async e=>{
        await this.setState({
            form:{
                ...this.state.form,
                [e.target.name]: e.target.value
            }
        });
        
    }

    iniciarSesion=async()=>{
        if(this.state.form.username!= '' && this.state.form.password!= ''){
            await axios.get(baseUrl, {params: {username: this.state.form.username, password: this.state.form.password}})
            .then(response=>{
                return response.data;
            })
            .then(response=>{
                if(response){
                    cookies.set('id', 1, {path: "/"});
                    cookies.set('apellido_paterno', "apellido1", {path: "/"});
                    cookies.set('apellido_materno', "apellido2", {path: "/"});
                    cookies.set('nombre', "nombre1", {path: "/"});
                    cookies.set('username', this.state.form.username, {path: "/"});
                    alert(`Bienvenido ${this.state.form.username} `);
                    window.location.href="./menu";
                }else{
                    alert('El usuario o la contraseña no son correctos');
                }
            })
            .catch(error=>{
                console.log(error);
            })
        }else{
            if(this.state.form.username=== ''){
            alert('El username  es requerido');}
            if(this.state.form.password=== ''){
                alert('El password  es requerido');}
        }

    }

    componentDidMount() {
        if(cookies.get('username')){
            window.location.href="./menu";
        }
    }
    
    render() {   
         
        return (             
            <div className="containerPrincipal">
                <div className="containerSecundario">
                        <div className="form-group">
                            <label>Usuario: </label>
                            <br />
                            <input
                            type="text"
                            className="form-control"
                            name="username"
                            onChange={this.handleChange} 
                            />
                            <br />
                            <label>Contraseña: </label>
                            <br />
                            <input
                            type="password"
                            className="form-control"
                            name="password"
                            onChange={this.handleChange} 
                            />
                            <br />
                            <button  className="btn btn-primary" onClick={()=> this.iniciarSesion()}>Iniciar Sesión</button>                        
                        </div>
                </div>
            </div>
        );
    
    }
}

export default Login;